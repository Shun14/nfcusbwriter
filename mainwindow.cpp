#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSerialPort/qserialport.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

int MainWindow::InitDll() {
    QLibrary loadLib("hfrdapi");
    if(loadLib.load()) {
        sys_open = (Sys_Open)loadLib.resolve("Sys_Open");
        sys_close= (Sys_Close)loadLib.resolve("Sys_Close");
        sys_isopen = (Sys_IsOpen)loadLib.resolve("Sys_IsOpen");
        sys_setlight = (Sys_SetLight)loadLib.resolve("Sys_SetLight");
        sys_getdevicenum = (Sys_GetDeviceNum)loadLib.resolve("Sys_GetDeviceNum");
        sys_setantenna = (Sys_SetAntenna)loadLib.resolve("Sys_SetAntenna");
        sys_inittype = (Sys_InitType)loadLib.resolve("Sys_InitType");
        sys_setbuzzer = (Sys_SetBuzzer)loadLib.resolve("Sys_SetBuzzer");
        sys_gethidserialnumberstr = (Sys_GetHidSerialNumberStr)loadLib.resolve("Sys_GetHidSerialNumberStr");
        tya_request = (TyA_Request)loadLib.resolve("TyA_Request");
        tya_ntag_anticollselect = (TyA_NTAG_AnticollSelect)loadLib.resolve("TyA_NTAG_AnticollSelect");
        tya_ntag_read = (TyA_NTAG_Read)loadLib.resolve("TyA_NTAG_Read");
        tya_ntag_pwdauth = (TyA_NTAG_PwdAuth)loadLib.resolve("TyA_NTAG_PwdAuth");
        tya_ntag_write = (TyA_NTAG_Write)loadLib.resolve("TyA_NTAG_Write");
        return RETURN_OK;
    } else {

        return RETURN_LOADDLLERROR;
    }
}

int MainWindow::operating() {
    if(RETURN_LOADDLLERROR == InitDll()) {
        QMessageBox::warning(NULL, tr("error"), QString("library no load"), QMessageBox::Yes);
        addLog(tr("library no load"));
        return RETURN_LOADDLLERROR;
    }
    int status = 0;
    DWORD devSum = 0;
    status = sys_getdevicenum(0x0416, 0x8020, &devSum);

    if(devSum == 0) {
        QMessageBox::warning(NULL, tr("error"), tr("reader no connected"), QMessageBox::Yes);
        addLog(tr("reader no connected"));
        return RETURN_LOADDLLERROR;
    }
    qDebug()<<"devSum:"<<devSum;
    QString imei = ui->showImeiText->text();

    if(sys_isopen(g_hDevice)) {
        if(imei.length() != 15) {
            qDebug()<<"imei error"<<imei;
            QMessageBox::warning(NULL, tr("错误"), tr("imei错误"), QMessageBox::Yes);
            addLog(tr("imei error"));
            return RETURN_LOADDLLERROR;
        }
        writeImei(imei);
    } else {
        status = sys_open(&g_hDevice, devSum-1,0x0416, 0x8020);
        if(status != RETURN_OK) {
            QMessageBox::warning(NULL, tr("error"), tr("device open failed"), QMessageBox::Yes);
            addLog(tr("device open failed"));
            return status;
        }
        if(imei.length() != 15) {
            QMessageBox::warning(NULL, tr("错误"), tr("imei错误"), QMessageBox::Yes);
            addLog(tr("imei error"));
            return RETURN_LOADDLLERROR;
        }
        writeImei(imei);
    }

    return true;
}

void MainWindow::writeImei(QString imei) {
    int status = 0;
    qDebug()<<"sys_open_status:"<<status;
    qDebug()<<g_hDevice;
    //唤醒读卡器
    status = sys_setlight(g_hDevice, RED);
    status = sys_setbuzzer(g_hDevice, 10);
    status = sys_setantenna(g_hDevice, 1);
    status = sys_inittype(g_hDevice, 'A');
    qDebug()<<"initType:"<<status;

    //唤醒卡片
    WORD pTagType = 0x0044;
    status = tya_request(g_hDevice, 0x52, &pTagType);
    qDebug()<<"request:"<<status;
    BYTE devStr[MAX_RF_BUFFER] = {0};
    BYTE devStrLength = 0;
    status = tya_ntag_anticollselect(g_hDevice, devStr, &devStrLength);
    qDebug()<<"devStrLength:"<<devStrLength<<"Status:"<<status;
    for(int i= 0; i< devStrLength; i++) {
        qDebug()<<"devStr:"<<devStr[i];
    }

    BYTE pData[3] = {0};
    BYTE pLen;
    //做初始认证，若为修改过密钥，则先认证再修改密钥
    status = tya_ntag_pwdauth(g_hDevice,modifiedKey, pData, &pLen);
    if(status!=0) {
        status = tya_request(g_hDevice, 0x52, &pTagType);
        qDebug()<<"request:"<<status;
        status = tya_ntag_anticollselect(g_hDevice, devStr, &devStrLength);
        status = tya_ntag_pwdauth(g_hDevice, key, pData, &pLen);
        status = tya_ntag_write(g_hDevice, 43, modifiedKey);
    }

    qDebug()<<"modifiedKey:"<<status;

//    BYTE value[17] = {0};
//    BYTE valueLen = 0;
//    status = tya_ntag_read(g_hDevice, 43, value, &valueLen);
//    qDebug()<<"valueLen:"<<valueLen;
//    for(int i = 0; i< valueLen; i++) {
//        qDebug()<<value[i];
//    }
    //给卡写入imei
    BYTE writeIMEI[24] ={0};
    writeIMEI[0] = 0x03;
    writeIMEI[1] = 0x14;
    writeIMEI[2] = 0xD1;
    writeIMEI[3] = 0x01;
    writeIMEI[4] = 0x10;
    writeIMEI[5] = 0x54;
    writeIMEI[6] = 0x00;
    for(int i=0;i<15; i++) {
        writeIMEI[i+7] = (unsigned char)imei.at(i).toLatin1();
    }

    int times = 6;
    for(int i = 0;i < times; i++) {
        BYTE desStr[4] = {0};
        for(int j = 0; j< 4;j++) {
            desStr[j] = writeIMEI[i*4 + j];
        }
        status = 0 || tya_ntag_write(g_hDevice, 4+i, desStr);
    }

    //写完卡给卡加密
    BYTE writeLock[] = {0x04, 0x00, 0x00,0x04};
    BYTE readLock[4] = {0};
    if(ui->readerListComboBox->currentIndex() == 1) {
        readLock[0] = 0xC0;//这里修改完成之后就再也无法修改
    } else {
        readLock[0] = 0x80;//这里需要使用密码读取以及修改
    }
    readLock[1] = 0x05;
    readLock[2] = 0x00;
    readLock[3] = 0x00;

    status = tya_ntag_write(g_hDevice, 41, writeLock);

    status = tya_ntag_write(g_hDevice, 42, readLock);
    qDebug()<<"lock"<<status;
    //判断状态
    if(status == 0) {
        QMessageBox::information(NULL, tr("提示"),tr("写入成功"), QMessageBox::Yes);
        addLog(tr("write data to card suc"));
    } else {
        QMessageBox::warning(NULL, "错误", "写入失败", QMessageBox::Yes, QMessageBox::Yes);
        addLog(tr("write data to card failed"));
    }
    qDebug()<<"status:"<<status;
}

void MainWindow::addLog(QString str) {
    QString now = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ddd");
    QString log = now + " " + str;
    ui->logWidget->addItem(log);
}


void MainWindow::keyPressEvent(QKeyEvent *event) {
    qDebug()<<event;
    //监听按键事件
    int tabIndex = ui->tabWidget->currentIndex() + 1;
    if(tabIndex == 1 && event->text() != "" && event->text() != "\r" && event->text() != "\n" && event->text() != "\t") {
        //event->key() == Qt::Key_Return &&
        IMEI += event->text();
    }

    //相当于按下enter后截取imei
    if(event->key() == Qt::Key_Return && tabIndex == 1) {
        QString arrInput = IMEI.section(QRegExp("[:;]"),5,5);
        qDebug()<<arrInput;
        ui->showImeiText->setText(arrInput);
        IMEI = "";
        operating();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_exportLabel_button_clicked()
{

    operating();
}

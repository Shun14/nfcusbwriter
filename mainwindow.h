#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLibrary>
#include <QSettings>
#include <QTime>
#include <QDateTime>
#include <QMessageBox>
#include <QKeyEvent>
#include <QDebug>


#define WINAPI      __stdcall
#define RETURN_OK 0              // Return OK
#define RETURN_LOADDLLERROR 3001 // Loaddll Error
#define HID_DEVICE_NOT_FOUND 0xC1
#define HID_DEVICE_NOT_OPENED 0xC2
#define HID_DEVICE_ALREADY_OPENED 0xC3 //设备已经打开
#define NONE 0X00
#define MAX_RF_BUFFER 1024

typedef void* HID_DEVICE;
typedef unsigned long       DWORD;
typedef unsigned char       BYTE;
typedef unsigned short      WORD;

//#define FAR far

//#ifndef _MANAGED
//#ifndef _MAC
//#ifdef _WIN64
//typedef INT_PTR (FAR WINAPI *FARPROC)();
//typedef INT_PTR (NEAR WINAPI *NEARPROC)();
//typedef INT_PTR (WINAPI *PROC)();
//#else
//typedef int (WINAPI *FARPROC)();
//typedef int (WINAPI *NEARPROC)();
//typedef int (WINAPI *PROC)();
//#endif  // _WIN64
//#else
//typedef int (CALLBACK *FARPROC)();
//typedef int (CALLBACK *NEARPROC)();
//typedef int (CALLBACK *PROC)();
//#endif
//#else
//typedef INT_PTR (WINAPI *FARPROC)(void);
//typedef INT_PTR (WINAPI *NEARPROC)(void);
//typedef INT_PTR (WINAPI *PROC)(void);
//#endif

typedef int(WINAPI* Sys_Open)(HID_DEVICE *device,
                              DWORD index,
                              WORD vid,
                              WORD pid);
typedef bool (WINAPI * Sys_IsOpen)(HID_DEVICE device);
typedef int (WINAPI * Sys_Close)(HID_DEVICE *device);
typedef int (WINAPI* Sys_SetLight)(HID_DEVICE device, BYTE color);
typedef int (WINAPI* Sys_SetBuzzer)(HID_DEVICE device, BYTE msec);
typedef int (WINAPI* Sys_SetAntenna)(HID_DEVICE device, BYTE mode);
typedef int (WINAPI* Sys_InitType)(HID_DEVICE device, BYTE type);
typedef int (WINAPI* Sys_GetDeviceNum)(WORD vid, WORD pid, DWORD *pNum);
typedef int (WINAPI* Sys_GetHidSerialNumberStr)(DWORD deviceIndex,
                                        WORD vid,
                                        WORD pid,
                                        char *deviceString,
                                        DWORD deviceStringLength);

typedef int (WINAPI* TyA_Request)(HID_DEVICE device, BYTE mode , WORD *pTagType);
typedef int (WINAPI* TyA_NTAG_AnticollSelect)(HID_DEVICE device, BYTE *pSnr, BYTE *pLen);
typedef int (WINAPI* TyA_NTAG_GetVersion)(HID_DEVICE device, BYTE *pData, BYTE* pLen);
typedef int (WINAPI* TyA_NTAG_Read)(HID_DEVICE device,
                                 BYTE addr,
                                 BYTE *pData,
                                 BYTE *pLen);

typedef int (WINAPI* TyA_NTAG_Write)(HID_DEVICE device, BYTE addr, BYTE *pdata);
typedef int (WINAPI* TyA_NTAG_PwdAuth)(HID_DEVICE device,
                                    BYTE *pPwd,
                                    BYTE *pData,
                                    BYTE *pLen);
typedef int (WINAPI* TyA_NTAG_ReadSig)(HID_DEVICE device,BYTE addr,BYTE *pData,BYTE *pLen);
typedef int (WINAPI* TyA_NTAG_CompWrite)(HID_DEVICE device, BYTE addr, BYTE *pData);

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_exportLabel_button_clicked();

private:
    int InitDll();
    void addLog(QString str);
    void keyPressEvent(QKeyEvent *event);
    int operating();
    void writeImei(QString imei);

    Ui::MainWindow *ui;
    QString IMEI = NULL;
    HID_DEVICE g_hDevice = HID_DEVICE(-1);
    int NO_COLOR = 0;
    int RED = 1;
    int YELLOW = 2;
    Sys_Open sys_open = NULL;
    Sys_IsOpen sys_isopen = NULL;
    Sys_Close sys_close = NULL;
    Sys_SetLight sys_setlight = NULL;
    Sys_SetBuzzer sys_setbuzzer = NULL;
    Sys_SetAntenna sys_setantenna = NULL;
    Sys_InitType sys_inittype = NULL;
    Sys_GetDeviceNum sys_getdevicenum = NULL;
    Sys_GetHidSerialNumberStr sys_gethidserialnumberstr = NULL;
    TyA_Request tya_request = NULL;
    TyA_NTAG_AnticollSelect tya_ntag_anticollselect = NULL;
    TyA_NTAG_Read tya_ntag_read = NULL;
    TyA_NTAG_Write tya_ntag_write = NULL;
    TyA_NTAG_PwdAuth tya_ntag_pwdauth = NULL;
    BYTE key[4]= {0xFF, 0xFF, 0xFF, 0xFF};
    BYTE modifiedKey[4] = {0xff, 0xff, 0xff, 0x1f};
};

#endif // MAINWINDOW_H
